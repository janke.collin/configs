#!powershell

$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/_lib/Lib"

$dividerChar = '='

Set-ExecutionPolicy Bypass -Scope Process -Force

Write-Output "Setting LocalMachine powershell execution policy to 'Bypass'"
Set-ExecutionPolicy -Scope LocalMachine Bypass

Write-Divider $dividerChar
Write-Output "windows: Begin Preliminary Windows Tweaks"

& "$ConfigDir\windows\disable-bluetooth.ps1"
& "$ConfigDir\windows\expand-explorer-ribbon.ps1"
& "$ConfigDir\windows\symlink-eval.ps1"
& "$ConfigDir\windows\symlink-no-admin.ps1"
& "$ConfigDir\windows\time-format.ps1"
& "$ConfigDir\windows\timezone.ps1"
& "$ConfigDir\windows\quickaccess-pin.ps1"

Write-Output "windows: Unpinning start menu and taskbar icons ($ConfigDir\windows\win10setup)"
& "$ConfigDir\windows\Unpin.cmd"

if ($env:USERNAME -eq "cj") {
  Write-Output "Setting FullName for user 'cj' to 'Collin Janke'"
  Set-LocalUser -Name cj -FullName "Collin Janke"
}

Write-Output "windows: End Preliminary Windows Tweaks"
Write-Divider $dividerChar

& "$ConfigDir\choco\setup.ps1"
Write-Divider $dividerChar

Write-Output "windows: Begin Main Windows Tweaks ($ConfigDir\windows\win10setup)"
& "$ConfigDir\windows\win10setup\Default.cmd"
Write-Output "windows: End Main Windows Tweaks ($ConfigDir\windows\win10setup)"
Write-Divider $dividerChar

# After reboot

# Fix for protonvpn
choco install -y protonvpn

Write-Divider $dividerChar
& "$ConfigDir\autohotkey\setup.ps1"
Write-Divider $dividerChar
& "$ConfigDir\cygwin\setup.ps1"
Write-Divider $dividerChar
& "$ConfigDir\firefox\setup.ps1"
Write-Divider $dividerChar
& "$ConfigDir\gitconfig\setup.ps1"
Write-Divider $dividerChar
& "$ConfigDir\virutalbox\setup.ps1"
Write-Divider $dividerChar
& "$ConfigDir\vscode\setup.ps1"
Write-Divider $dividerChar
& "$ConfigDir\windows\terminal\setup.ps1"
Write-Divider $dividerChar
