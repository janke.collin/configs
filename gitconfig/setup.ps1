#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "gitconfig: Begin gitconfig Setup ($ThisDir)"

$cygwinDir = "C:\tools\cygwin64"
$cygwinHomeDir = "$cygwinDir\home\$env:USERNAME"

New-Symlink -Link (Join-Path $env:USERPROFILE ".gitconfig") -Target "$ThisDir\gitconfig"

if (Test-Path $cygwinDir) {
  # Create home skel if not created
  if (!(Test-Path $cygwinHomeDir)) {
    & "$cygwinDir\bin\bash.exe" --login -c "ls -la"
  }

  New-Symlink -Link "$cygwinHomeDir\.gitconfig" -Target "$ThisDir\gitconfig"
  New-Symlink -Link "$cygwinHomeDir\shell.gitconfig" -Target "$ThisDir\cygwin.shell.gitconfig"
}

Write-Output "gitconfig: End gitconfig Setup ($ThisDir)"
