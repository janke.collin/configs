#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "choco: Begin Chocolatey Setup ($ThisDir)"

if (!(Get-Command -ErrorAction SilentlyContinue choco)) {
  Write-Host "choco: Installing Chocolatey"

  # https://chocolatey.org/install
  Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
  Write-Divider
}

Write-Output "choco: Installing Chocolatey Packages ($ThisDir\packages.config)"
choco install -y -f "$ThisDir\packages.config"
Write-Divider

Write-Output "choco: Installing Cygwin Packages ($ThisDir\cygwin.config)"
choco install -y -s cygwin -f "$ThisDir\cygwin.config"
Write-Divider

Write-Output "choco: Installing Windows Features ($ThisDir\windowsfeatures.config)"
choco install -y -s windowsfeatures -f "$ThisDir\windowsfeatures.config"
Write-Divider

Write-Output "choco: Refreshing Environment"
$env:ChocolateyInstall = Convert-Path "$((Get-Command choco).Path)\..\.."
Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
Update-SessionEnvironment

Write-Output "choco: Removing desktop shortcuts"
Remove-Item -Verbose (Join-Path $env:PUBLIC "Desktop\*.lnk")
Remove-Item -Verbose (Join-Path $env:USERPROFILE "Desktop\*.lnk")

Write-Output "choco: End Chocolatey Setup ($ThisDir)"
