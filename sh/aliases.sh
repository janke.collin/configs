#!/bin/sh

# Customization
alias diff="diff --color=auto"
alias grep="grep --color=auto"
alias ls="ls --color=auto"

# Shortcuts
alias ll="ls -l"
alias lla="ls -lA"

# Typos
alias got="git"
alias gut="git"
alias sl="ls"
alias clera="clear"
alias cd..="cd .."
