#!/bin/bash

if [ "$(whoami)" != 'root' ]; then
  echo 'This script must be run as "root".'
  exit 1
fi

# Absolute path to this script
SCRIPT=$(readlink -f $0)
# Absolute path to directory this script is in
SCRIPTPATH=`dirname $SCRIPT`

echo "Hardening SSHD..."

echo "Copying new config"
cp -v "$SCRIPTPATH/sshd_config" "/etc/ssh/sshd_config"

echo "Regenerating keys"
rm /etc/ssh/ssh_host_*
ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ""
ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""

echo "Disabling small moduli"
awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.safe
mv /etc/ssh/moduli.safe /etc/ssh/moduli

echo "Done."
echo "Restart the SSH server for changes to take effect (service ssh restart)."
