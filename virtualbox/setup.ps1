#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "virtualbox: Begin VirtualBox Setup ($ThisDir)"

$vboxDir = (Join-Path $env:ProgramFiles "Oracle\VirtualBox")
$vboxManage = (Join-Path $vboxDir "VBoxManage")
$vmDir = (Join-Path $env:USERPROFILE "vm")

Write-Output "virtualbox: Setting default vm directory: $vmDir"
& $vboxManage setproperty machinefolder $vmDir

Write-Output "virtualbox: Removing Host-Only Adapters"
Get-NetAdapter |
Where-Object "Name" -Like "*virtualbox*" |
ForEach-Object {
  $iface = $_.InterfaceDescription
  Write-Output "Removing $iface"
  & $vboxManage hostonlyif remove "$iface"
}

Write-Output "virtualbox: End VirtualBox Setup ($ThisDir)"
