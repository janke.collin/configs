#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "autohotkey: Begin AutoHotkey Setup ($ThisDir)"

$startupDir = (Join-Path $env:APPDATA "Microsoft\Windows\Start Menu\Programs\Startup")

Write-Output "autohotkey: Enabling super paste at startup"
New-Symlink -Link "$startupDir\super-paste.ahk" -Target "$ThisDir\super-paste.ahk"

Write-Output "autohotkey: End AutoHotkey Setup ($ThisDir)"
