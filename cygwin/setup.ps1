#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "cygwin: Begin Cygwin Setup ($ThisDir)"

$cygwinDir = "C:\tools\cygwin64"
$cygwinBash = (Join-Path $cygwinDir "bin\bash")

& $cygwinBash --login -c "`$(cygpath -u '$ThisDir\cygdrive.sh')"

Write-Output "cygwin: End Cygwin Setup ($ThisDir)"
