#!powershell
function PinToQuickAccess {
  param ($path)

  $o = new-object -com shell.application
  $o.Namespace($path).Self.InvokeVerb("pintohome")
}

PinToQuickAccess $env:USERPROFILE
PinToQuickAccess "C:\code"
