@echo off

powershell.exe -NoProfile -ExecutionPolicy Bypass -File "%~dp0win10setup\Win10.ps1" -include "%~dp0win10setup\Win10.psm1" -preset "%~dpn0.preset"

taskkill /im explorer.exe /f
start explorer.exe
