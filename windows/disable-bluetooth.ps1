#!powershell
# https://www.reddit.com/r/PowerShell/comments/ckd63z/disabling_bluetooth_device_via_powershell/
(GWMI Win32_PNPEntity -Filter "caption like '%bluetooth%'").Disable()
