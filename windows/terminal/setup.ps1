$configDir = "C:\code\conf\configs"
$terminalLocalState = (Join-Path $env:LOCALAPPDATA "Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState")

New-Item -Force -Type SymbolicLink -Path "$terminalLocalState\settings.json" -Target "$configDir\windows\terminal\settings.json" | Out-Null
