#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "vscode: Begin VS Code Setup ($ThisDir)"

$vscodeUserDir = (Join-Path $env:APPDATA "Code\User")

New-Directory $vscodeUserDir

New-Symlink -Link "$vscodeUserDir\settings.json" -Target "$ThisDir\settings.json"
New-Symlink -Link "$vscodeUserDir\keybindings.json" -Target "$ThisDir\keybindings.json"
Write-Divider
Get-Content "$ThisDir\extensions.list" | ForEach-Object { code --install-extension $_ }

Write-Output "vscode: End VS Code Setup ($ThisDir)"
