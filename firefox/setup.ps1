#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path
Import-Module "$ThisDir/../_lib/Lib"

Write-Output "firefox: Begin Firefox Setup ($ThisDir)"

$firefoxDir = (Join-Path $env:APPDATA "Mozilla\Firefox")
$firefoxProfile = (Join-Path $firefoxDir "Profiles\yuom5bir.default-release")

New-Item -Force -Type SymbolicLink -Path "$firefoxProfile\user.js" -Target "$ThisDir\user.js" | Out-Null
Remove-Item -Force -ErrorAction SilentlyContinue "$firefoxProfile\prefs.js"

Write-Output "firefox: End Firefox Setup ($ThisDir)"
