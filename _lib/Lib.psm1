#!powershell
$ThisDir = Split-Path $MyInvocation.MyCommand.Path

function Set-Globals {
  if (!($ConfigDir)) {
    $global:ConfigDir = Split-Path -Parent $ThisDir
    Write-Host "ConfigDir: $ConfigDir"
  }
  if (!($LogDir)) {
    $global:LogDir = Join-Path $ConfigDir "log"
    Write-Host "LogDir: $LogDir"
  }
}


function Get-AbsolutePath {
  param (
    $path,
    $base
  )

  if (!($base)) {
    $base = (Get-Location)
  }

  Write-Host "Base: $base"
  Push-Location $base | Out-Null
  $r = Resolve-Path -LiteralPath $path
  Pop-Location | Out-Null
  Write-Output $r
}

function Get-RelativePath {
  param (
    $path,
    $base
  )

  if (!($base)) {
    $base = (Get-Location)
  }

  Write-Host "Base: $base"
  Push-Location $base | Out-Null
  $r = Resolve-Path -Relative -LiteralPath $path
  Pop-Location | Out-Null
  Write-Output $r
}

function New-Directory {
  param (
    $dir
  )

  if (Test-Path $dir) {
    Write-Information "Directory $dir already exists"
  } else {
    Write-Information "Creating directory: $dir"
    if (!(New-Item -Force -Type Directory $dir)) {
      Write-Error "Failed to create directory: $dir"
    }
  }
}

function New-Symlink {
  param (
    $link,
    $target
  )

  Write-Output "Symlinking $link -> $target"

  if (Test-Path $link) {
    Write-Warning "File already exists. Overwriting $link"
  }
  if (!(Test-Path $target)) {
    Write-Warning "Symlink target does not exist: $target"
  }

  New-Item -Force -Type SymbolicLink -Path $link -Target $target | Out-Null
}

function Write-Divider {
  param (
    $char,
    $count
  )

  if (!$char) { $char = '-' }
  if (!$count) { $count = 60 }
  Write-Output ($char * $count)
}

### Run on Import ###
Set-Globals
